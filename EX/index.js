// EX1: Tính ngày tháng năm

function ngayHomQua() {
  var nhapNgay = document.getElementById("nhapNgay").value * 1;
  var nhapThang = document.getElementById("nhapThang").value * 1;
  var nhapNam = document.getElementById("nhapNam").value * 1;

  var ketQua;
  var ngayHomQua;
  var thangN;
  var namN;
  // if(nhapNam % 4==0 && nhapNam % 100 != 0 || nhapNam % 400 == 0 ){

  // }
  ngayHomQua = nhapNgay - 1;
  thangN = nhapThang;
  namN = nhapNam;
  // Tính năm
  if (nhapThang == 1) {
    if (nhapNgay == 1) {
      namN = nhapNam - 1;
      thangN = 12;
    }
  }
  //
  if (nhapThang == 5 || nhapThang == 7 || nhapThang == 10 || nhapThang == 12) {
    if (nhapNgay == 1) {
      ngayHomQua = 30;
      thangN = nhapThang - 1;
    }
  } else if (
    nhapThang == 1 ||
    nhapThang == 4 ||
    nhapThang == 6 ||
    nhapThang == 8 ||
    nhapThang == 9 ||
    nhapThang == 11 ||
    nhapThang == 2
  ) {
    if (nhapNgay == 1) {
      ngayHomQua = 31;
      thangN = nhapThang - 1;
    }
  } else {
    if (nhapNgay == 1) {
      if ((nhapNam % 4 == 0 && nhapNam % 100 != 0) || nhapNam % 400 == 0) {
        ngayHomQua = 29;
        thangN = nhapThang - 1;
      } else {
        ngayHomQua = 28;
        thangN = nhapThang - 1;
      }
    }
  }

  ///////

  document.getElementById("ketQua").innerHTML =
    ngayHomQua + "/" + thangN + "/" + namN;
}

function ngayMai() {
  var nhapNgay = document.getElementById("nhapNgay").value * 1;
  var nhapThang = document.getElementById("nhapThang").value * 1;
  var nhapNam = document.getElementById("nhapNam").value * 1;

  var ketQua;
  var ngayMai = nhapNgay + 1;
  var thangN = nhapThang;
  var namN = nhapNam;
  // Cuối năm
  if (nhapThang == 2) {
    if (nhapNgay > 29 || nhapThang > 12) {
      alert("Dữ liệu không đúng");
    }
  } else {
    if (nhapNgay > 31 || nhapThang > 12) {
      alert("Dữ liệu không đúng");
    }
  }

  if (
    nhapThang == 1 ||
    nhapThang == 3 ||
    nhapThang == 5 ||
    nhapThang == 7 ||
    nhapThang == 8 ||
    nhapThang == 10 ||
    nhapThang == 12
  ) {
    if (nhapNgay == 31) {
      ngayMai = 1;
      thangN = nhapThang + 1;
    }
  } else if (
    nhapThang == 4 ||
    nhapThang == 6 ||
    nhapThang == 9 ||
    nhapThang == 11
  ) {
    if (nhapNgay == 30) {
      ngayMai = 1;
      thangN = nhapThang + 1;
    }
  } else if ((nhapNam % 4 == 0 && nhapNam % 100 != 0) || nhapNam % 400 == 0) {
    if (nhapNgay == 29) {
      ngayMai = 1;
      thangN = nhapThang + 1;
    }
    if (nhapNgay == 28) {
      ngayMai = 1;
      thangN = nhapThang + 1;
    }
  }
  if (nhapThang == 12) {
    if (nhapNgay == 31) {
      namN = nhapNam + 1;
      thangN = 1;
    }
  }

  document.getElementById("ketQua").innerHTML =
    ngayMai + "/" + thangN + "/" + namN;
}

//--------------------------------------------------------

//EX2: Tính ngày
function tinhNgay() {
  var nhapThang = document.getElementById("nhapSoThang").value * 1;
  var nhapNam = document.getElementById("nhapSoNam").value * 1;

  var soNgay;

  if (
    nhapThang == 1 ||
    nhapThang == 3 ||
    nhapThang == 5 ||
    nhapThang == 7 ||
    nhapThang == 8 ||
    nhapThang == 10 ||
    nhapThang == 12
  ) {
    soNgay = 31;
  } else if (
    nhapThang == 4 ||
    nhapThang == 6 ||
    nhapThang == 9 ||
    nhapThang == 11
  ) {
    soNgay = 30;
  } else {
    if ((nhapNam % 4 == 0 && nhapNam % 100 != 0) || nhapNam % 400 == 0) {
      soNgay = 29;
    } else {
      soNgay = 28;
    }
  }

  document.getElementById(
    "ketQua1"
  ).innerHTML = `Tháng ${nhapThang} năm ${nhapNam} có: ${soNgay} ngày`;
}

//---------------------------------------------

// EX3: Đọc số

function docSo() {
  var nhapSo = document.getElementById("number").value * 1;

  var soHangDonVi = nhapSo % 10;
  var soHangChuc = Math.floor(nhapSo / 10) % 10;
  var SoHangTram = Math.floor(nhapSo / 100);

  var x = SoHangTram;
  var y = soHangChuc;
  var z = soHangDonVi;

  if (nhapSo > 999) {
    alert("Dữ liệu nhập không đúng");
    return;
  }

  if (x == 1) {
    x = "một";
  } else if (x == 2) {
    x = "hai";
  } else if (x == 3) {
    x = "ba";
  } else if (x == 4) {
    x = "bốn";
  } else if (x == 5) {
    x = "năm";
  } else if (x == 6) {
    x = "sáu";
  } else if (x == 7) {
    x = "bảy";
  } else if (x == 8) {
    x = "tám";
  } else if (x == 9) {
    x = "chín";
  }
  //------------------
  if (y == 1) {
    y = "một";
  } else if (y == 2) {
    y = "hai";
  } else if (y == 3) {
    y = "ba";
  } else if (y == 4) {
    y = "bốn";
  } else if (y == 5) {
    y = "năm";
  } else if (y == 6) {
    y = "sáu";
  } else if (y == 7) {
    y = "bảy";
  } else if (y == 8) {
    y = "tám";
  } else if (y == 9) {
    y = "chín";
  }
  //----------------
  if (z == 1) {
    z = "một";
  } else if (z == 2) {
    z = "hai";
  } else if (z == 3) {
    z = "ba";
  } else if (z == 4) {
    z = "bốn";
  } else if (z == 5) {
    z = "năm";
  } else if (z == 6) {
    z = "sáu";
  } else if (z == 7) {
    z = "bảy";
  } else if (z == 8) {
    z = "tám";
  } else if (z == 9) {
    z = "chín";
  }

  document.getElementById("ketQua2").innerHTML = `${x} trăm ${y} mươi ${z}`;
}

//-------------------------------------------------------

//EX4: Tìm sinh viên xa trường nhất

function timQuangDuong() {
  var nhapTen = document.getElementById("nhapTen").value;
  var nhapTen2 = document.getElementById("nhapTen2").value;
  var nhapTen3 = document.getElementById("nhapTen3").value;

  var nhapX = document.getElementById("nhapX").value * 1;
  var nhapY = document.getElementById("nhapY").value * 1;

  var nhapX2 = document.getElementById("nhapX2").value * 1;
  var nhapY2 = document.getElementById("nhapY2").value * 1;

  var nhapX3 = document.getElementById("nhapX3").value * 1;
  var nhapY3 = document.getElementById("nhapY3").value * 1;

  var nhapX4 = document.getElementById("nhapX4").value * 1;
  var nhapY4 = document.getElementById("nhapY4").value * 1;

  var d = 0;
  var d2 = 0;
  var d3 = 0;
  var dmax = 0;

  d = Math.sqrt(Math.pow(nhapX4 - nhapX, 2) + Math.pow(nhapY4 - nhapY, 2));
  d2 = Math.sqrt(Math.pow(nhapX4 - nhapX2, 2) + Math.pow(nhapY4 - nhapY2, 2));
  d3 = Math.sqrt(Math.pow(nhapX4 - nhapX3, 2) + Math.pow(nhapY4 - nhapY3, 2));

  if (d < d2) {
    if (d2 < d3) {
      dmax = d3;
    } else {
      dmax = d2;
    }
  } else {
    if (d < d3) {
      dmax = d3;
    } else {
      dmax = d;
    }
  }

  if (dmax == d) {
    document.getElementById(
      "ketQua3"
    ).innerHTML = `Sinh viên xa trường nhất ${nhapTen}`;
  } else if (dmax == d2) {
    document.getElementById(
      "ketQua3"
    ).innerHTML = `Sinh viên xa trường nhất ${nhapTen2}`;
  } else {
    document.getElementById(
      "ketQua3"
    ).innerHTML = `Sinh viên xa trường nhất ${nhapTen3}`;
  }
}
